'use strict';

/**
 * To see the full list of Audits in the console run:
 * "lighthouse --list-all-audits"
 */

module.exports = {
  extends: 'lighthouse:default',
  settings: {
    onlyAudits: [
            "offscreen-images",
            "uses-optimized-images",
            "uses-request-compression",
            "uses-responsive-images",
            "uses-webp-images",
            "image-aspect-ratio",
            "external-anchors-use-rel-noopener",
    ],
  },
};