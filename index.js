// Simple Site Audits
'use strict';

const Lighthouse = require('lighthouse');
const ChromeLauncher = require('lighthouse/chrome-launcher/chrome-launcher');
const log = require('lighthouse-logger');
const fs = require('fs');
const csv = require('csvtojson');

// This config file defines the specific audits you wish to run
const auditCriteria = require('./audits-configs/images-audit.js');

// FOR DEBUGGING
// console.log(util.inspect(myObject, false, null));
const util = require('util');

const flags = {
    logLevel: 'info',
    chromeFlags: ['--headless']
};

const siteReport = {};

/**
 * runLighthouseAudit
 * Runs the Lighthouse Audit on multiple pages in a recursive manner.
 * Recursive prevents Lighthouse from launching multiple instances of Chrome on your machine.
 *
 * Note: Perhaps there could be a way to limit the number of instances launched to help speed things up
 *
 * @param pageUrls: JSON Object
 * @param flags: Object of Lighthouse options
 * @param auditCriteria: Custom Lighthouse Audit Options
 * @returns {Promise<any>}
 */
const runLighthouseAudit = async function (pageUrls, flags, auditCriteria = null) {

    return new Promise(async function (resolve, reject) {

        // Launch Chrome and grab the port
        const chrome = await ChromeLauncher.launch();
        flags.port = chrome.port;

        console.log("\n---\n" + "Auditing: " + pageUrls[0].Address + "\n" + pageUrls.length + " urls left to audit" + "\n---\n")

        const results = await Lighthouse(pageUrls[0].Address, flags, auditCriteria);
        const audits = results.audits;
        await chrome.kill();

        // Take the results and filter for siteReport Obj
        Object.keys(audits).forEach(async (audit, i) => {

            if (typeof audits[audit].score === 'number' && audits[audit].score < 100
                || typeof audits[audit].score === 'boolean' && audits[audit].score === 'false') {

                siteReport[audit] = siteReport[audit] || [];
                siteReport[audit].description = siteReport[audit].description || audits[audit].description;
                siteReport[audit].helpText = siteReport[audit].helpText || audits[audit].helpText;

                let siteReportObj = {
                    url: pageUrls[0].Address,
                    score: audits[audit].score,
                }

                // Pull out the individual urls for report detail
                const auditItems = await auditOffscreenImages(results.audits[audit]);
                siteReportObj.details = auditItems;

                siteReport[audit].push(siteReportObj);

            }
        });

        // Remove the current url for the recursive call
        pageUrls.shift();

        if (pageUrls.length > 0) {
            resolve(runLighthouseAudit(pageUrls, flags, auditCriteria));
        } else {
            resolve(siteReport);
        }
    });

}

/**
 * auditOffscreenImages
 * @param object
 * @returns Array
 */
const auditOffscreenImages = async function (object) {

    let urlArray = object.details.items.map(itemsObj => {

        if (itemsObj[0].hasOwnProperty("url")) {
            return itemsObj[0].url;
        }

        if (itemsObj[0].hasOwnProperty("text")) {
            return itemsObj[0].text;
        }

    });

    return urlArray;
}

/**
 * startAudit
 * Takes the csv file from the input and starts the process for running Lighthouse on multiple pages
 * @returns The final report file
 */

const startAudit = async function () {

    // Retrieve filename
    try {
        if (process.argv.slice(2).length === 0) {
            throw(new Error('Please provide a CSV Filename as the 3rd param'))
        }
    }
    catch (error) {
        console.error(error.message);
        return;
    }

    const csvFilePath = process.argv.slice(2);

    // We run the CSV to remove the first line if the csv is exported from Screaming Frog
    const pageUrls = await csv().fromFile(csvFilePath[0]).preRawData((csvRawData) => {
        let newData = csvRawData.replace(/"[A-Za-z\s-]{0,}"\n/g, '');
        return newData;
    });

    // Sitename string to append to filename
    let siteUrl = pageUrls[0].Address;
    let siteUrlArray = siteUrl.split('/');
    let siteName = '_' + siteUrlArray[2];

    //Run the audits
    const siteReportObj = await runLighthouseAudit(pageUrls, flags, auditCriteria);
    const linksToProcess = [];

    // Lets write the report
    let reportOutput = "# Site Report for: " + siteName + "\n\n";

    // Process the data to ouput individual reports
    Object.keys(siteReportObj).forEach(async (audit) => {

        reportOutput += "---\n";
        reportOutput += "## Failed Audit: " + audit + "\n\n";
        reportOutput += "**" + siteReportObj[audit].description + "**\n\n";
        reportOutput += siteReportObj[audit].helpText + "\n\n";
        reportOutput += "---\n";

        siteReportObj[audit].map(async (page) => {

            reportOutput += "Page: " + page.url + " | Score: " + page.score + "  \n";
            if (page.hasOwnProperty('details')) {
                reportOutput += "\n" + audit + "\n";
                page.details.map((image) => {
                        reportOutput += image + "\n";
                        if(audit === "uses-webp-images" && linksToProcess.indexOf(image) === -1){
                            linksToProcess.push(image)
                        }
                });
                reportOutput += "---\n\n";
            }

        });


        reportOutput += "\n";
    });

    fs.writeFileSync('site-audit-report_' + siteName + '.md', reportOutput, 'utf8');

    if(linksToProcess.length > 0){
        let linkToProcessOutput = "\"strict\"\n\nmodule.exports = [\n";
        linksToProcess.forEach( link => {
            linkToProcessOutput += "\t'" + link + "',\n";
        });
        linkToProcessOutput += "]"

        fs.writeFileSync('images_to_kraken_' + siteName + '.js', linkToProcessOutput, 'utf8');
    }

};

startAudit();
