# Lighthouse Bulk Site Audit Script
This script allows you to run a tailored audit on multiple urls provided via a .csv file.

## Output
Currently the script will only flag the urls that have failed to have a Lighthouse score of 100. The urls will be 
listed under the specific Audit heading. The future plans are to extend this to extract more data provided by Lighthouse. 

## Enviroment Req
NodeJs V7.6+
Async functions are not supported by Node versions older than version 7.6.

## 1: Preparing your .csv file
Use Screaming Frog to crawl and retrieve the complete list of urls for a site. After the Screaming Frog has finished,
 select the `internal` tab, filter by `HTML` and export to results to csv.
 
By default Screaming Frog will insert a top row heading like `Internal HTML`. Open the .csv file in either excel 
or a text editor and remove this top row. Save the csv file to the root of this folder. 

## 2: Script Setup
```
npm install
```

## 3: Running the script

```
node index.js your-csv-file.csv
```
Note: if the csv file param is not provided you will be notified in the console.

## Configuration
The script has been setup to run any of the audits available within Lighthouse. The audit file is stored in 
`audit-configs/`.

All of the audits available in Lighthouse can be listed by running `lighthouse --list-all-audits` in the console.

It should be noted that audits listed with a `/` only need the subname, ie `manual/pwa-cross-browser` would be the 
audit `pwa-cross-browser`.

**ref:** https://github.com/GoogleChrome/lighthouse/tree/master/lighthouse-core/audits

## Resources
[Lighthouse Setup Tutorial](https://developers.google.com/web/tools/lighthouse/)

[Creating your own custom audits](https://github.com/GoogleChrome/lighthouse/tree/master/docs/recipes/custom-audit)

[Understanding the Lighthouse JSON Results](https://github.com/GoogleChrome/lighthouse/blob/master/docs/understanding-results.md)



